/* eslint-disable no-console */

import { register } from 'register-service-worker'

//comprueba el entorno, solo se aplica en producción
if (process.env.NODE_ENV === 'production') {
  //registra el service worker.
  register(`${process.env.BASE_URL}service-worker.js`, {
    //indica que el service worker está siendo utilizado
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    registered () {
      console.log('Service worker has been registered.')
    },
    //se dispara cuando se ha obtenido información cacheada para el uso offline
    cached () {
      console.log('Content has been cached for offline use.')
    },
    //se localiza contenido
    updatefound () {
      console.log('New content is downloading.')
    },
    //se actualiza contenido
    updated () {
      console.log('New content is available; please refresh.')
    },
    // que ejecutar cuando no hay conexión a internet
    offline () {
      console.log('No internet connection found. App is running in offline mode.')
    },
    // que ejecutar con un error
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  })
}
